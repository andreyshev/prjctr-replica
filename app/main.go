package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
)

func main() {
	go sendQueryes()
	awaitTermination(context.TODO())
	fmt.Println("Success!")
}

func awaitTermination(ctx context.Context) {
	// Listen to interrupt signal
	ctx, stop := signal.NotifyContext(ctx, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer func() {
		stop()
		if errors.Is(ctx.Err(), context.Canceled) {
			return
		}
	}()
	<-ctx.Done()
}

func sendQueryes() {
	db, err := sql.Open("mysql", "root:123@tcp(127.0.0.1:440606)/testdb")
	if err != nil {
		panic(err.Error())
	}

	defer db.Close()
	timer := time.NewTimer(10 * time.Minute)
	for {
		select {
		case <-timer.C:
			return
		default:
			title, _ := uuid.NewUUID()
			dir, _ := uuid.NewUUID()
			year := 2003
			if _, err := db.Exec("INSERT INTO movies VALUES (?,?,?)", title.String(), dir.String(), year); err != nil {
				log.Println(err)
			}

			time.Sleep(1 * time.Second)
		}
	}
}
